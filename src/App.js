// import logo from './logo.svg';
import './App.css';
import { RouteConfig } from './router'
import { useRoutes } from 'react-router-dom'

function App() {
    let Elements = useRoutes(RouteConfig);

    return (
        <div className="App">
            {Elements}
        </div>
    );
}

export default App;
