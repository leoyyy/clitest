import hfetch from './hfetch';

const services = {
    getHomeDetail: (params) => hfetch.post('/home', params),

    getCells: (params) => hfetch.get('/getCellListByParams', {params: params}),
};

export default services
