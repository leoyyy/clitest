import { useEffect } from "react"
import { useRequest } from "ahooks"
import services from "@/services/index"

export default () => {
    const { data, error, loading } = useRequest(services.getHomeDetail, {})
    if(error) return <div>error</div>

    if(loading) return <h2>loading</h2>

    return (<h1>{data.code}</h1>)
}